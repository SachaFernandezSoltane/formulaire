<?php
function test($tableau){
  foreach ($tableau as $value) {
    if(!isset($_POST[$value])|| ($_POST[$value]=='')){
      return false;
    }
  }
  return true;
}
$tableau=['email','mdp'];
if(test($tableau)){
  $EmailUser=$_POST['email'];
  $MotDePasse=$_POST['mdp'];
  require 'credentials.php';
  //Etape 1: Connexion à la base de données
  try{
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $resultats = $dbh->prepare("SELECT id,prenom FROM utilisateurs WHERE email=:email AND mot_de_passe=:mot_de_passe");
    $resultats->bindParam(':email',$EmailUser);
    $resultats->bindParam(':mot_de_passe',$MotDePasse);
    $resultats->execute();
    $resultats=$resultats->fetchAll();
    if(count($resultats)){
      header('location:connexion.php');
    }else{
      echo "aucun utilisateurs";
    }
  }
  catch(Exception $e){
    var_dump($e->getMessage());
  }
}else{
  die("impossible de charger la page");
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  </body>
</html>
