<?php
function test($tableau){
  foreach ($tableau as $value) {
    if(!isset($_POST[$value])|| ($_POST[$value]=='')){
      return false;
    }
  }
  return true;
}
$tableau=["Id"];
if(test($tableau)){
  require 'credentials.php';
  //Etape 1: Connexion à la base de données
  try{
    $IdChoisi=$_POST["Id"];
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $resultats = $dbh->prepare("DELETE FROM utilisateurs WHERE id=:id");
    $resultats->bindParam(':id',$IdChoisi);
    $resultats -> execute();
    if($resultats){
      header('location:index.php');
    }else{
      echo "erreur";
    }
  }
  catch(Exception $e){
    var_dump($e->getMessage());
  }
}else{
  die("impossible de supprimer");
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  </body>
</html>
