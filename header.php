<?php
  session_start();
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark sticky-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php">Home </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse navbar-right justify-content-end" id="navbarNavDarkDropdown">
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link" href="connexion.php" id="navbarDarkDropdownMenuLink" role="button" data-bs-hover="dropdown" aria-expanded="false">
            <input type="button" class="btn btn-primary" value="Se connecter">
          </a>
        </li>
      </ul>
    </div>
  <div>
</nav>
