<?php
function test($tableau){
  foreach ($tableau as $value) {
    if(!isset($_GET[$value])||($_GET[$value])==""){
      return false;
    }
  }
  return true;
}
$tableau=["id"];
if(test($tableau)){
  require 'credentials.php';
  //Etape 1: Connexion à la base de données
  try{
    $IdUser=$_GET["id"];
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $resultats = $dbh->prepare("SELECT id,nom,prenom FROM utilisateurs
                                       WHERE id=:id");
    $resultats->bindParam(":id",$IdUser);
    $resultats->execute();
    $result=$resultats->fetchAll();
    if(count($result)==0){
      header("location:index.php");
      die("impossible");
    }
  }
  catch(Exception $e){
    var_dump($e->getMessage());
  }
}else{
  header("location:index.php");
  die("Impossible de charger la page");
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Update</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>

<body>
  <?php require 'header.php';
  ?>
  <div class="container">
    <div class="row">
      <h1 class="col-12 text-center">TABLEAU DES UTILISATEURS - UPDATE</h1>
        <table class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>nom</th>
              <th>prenom</th>
              <th>Envoyer</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($result as $value) {?>
            <tr>
                <form class="" action="traitement-update.php" method="post">
                    <td><input class="input-group-text" type="text" value="<?php echo $value['nom']?>" name="nom"></td>
                    <td><input class="input-group-text" type="text" value="<?php echo $value['prenom']?>" name="prenom"></td>
                    <?php
                    $_SESSION["id"]=$value['id'];
                    $_SESSION["nom"]=$value['nom'];
                    $_SESSION["prenom"]=$value['prenom'];
                     ?>
                    <td><input class="btn btn-primary" type="submit" value="Mettre à jour"/></td>
                </form>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
    </div>
  </div>

  <?php require 'footer.php'; ?>
</body>

</html>
