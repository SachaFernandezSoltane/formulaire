<?php
require 'credentials.php';
session_start();
//Etape 1: Connexion à la base de données
if($_SESSION["nom"]!=$_POST["nom"] || $_SESSION["prenom"]!=$_POST["prenom"]){
  try{
    $IdUser=$_SESSION["id"];
    $NomUser=$_POST["nom"];
    $PrenomUser=$_POST["prenom"];
    require 'credentials.php';
    $dbh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
    $resultats = $dbh->prepare("UPDATE utilisateurs
                                SET nom=:nom
                                , prenom=:prenom
                                WHERE id=:id");
    $resultats->bindParam(":id",$IdUser);
    $resultats->bindParam(":nom",$NomUser);
    $resultats->bindParam(":prenom",$PrenomUser);
    $resultats->execute();
    $_SESSION["nom"]=$NomUser;
    $_SESSION["prenom"]=$PrenomUser;
    header("location:index.php");
  }
  catch(Exception $e){
    var_dump($e->getMessage());
  }
}
header("location:index.php");
?>
